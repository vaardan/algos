package algos

type prioritizable interface {
	Priority() int
}

// MinHeap a weakly balanced tree. Minimal element is always at the top.
// Usually implemented using an array list. Because of that tree is always complete (self-balanced).
// O(logN) on inserts and deletes, because it's complete and in the worst case logN swaps must be performed to bubble up/down.
// On push: add element to the end, then bubble up and swap if parent is greater than the current value.
// On pop: save root. If it wasn't the last element, then swap last element with the root and bubble it down.
type MinHeap[T prioritizable] struct {
	values []T
}

func NewMinHeap[T prioritizable]() *MinHeap[T] {
	return &MinHeap[T]{}
}

func (h *MinHeap[T]) Push(v T) {
	h.values = append(h.values, v)

	lenAfterPush := len(h.values)
	if lenAfterPush != 1 {
		h.heapifyUp(lenAfterPush - 1)
		return
	}
}

func (h *MinHeap[T]) Pop() *T {
	lenBeforePop := len(h.values)
	if lenBeforePop == 0 {
		return nil
	}

	topElement := h.values[0]
	if lenBeforePop == 1 {
		h.values = make([]T, 0)
		return &topElement
	}

	h.values[0] = h.values[lenBeforePop-1]
	h.values = h.values[:lenBeforePop-1]

	h.heapifyDown(0)

	return &topElement
}

func (h *MinHeap[T]) Peek() *T {
	if len(h.values) == 0 {
		return nil
	}

	return &h.values[0]
}

func (h *MinHeap[T]) Len() int {
	return len(h.values)
}

func (h *MinHeap[T]) heapifyUp(index int) {
	// it's already the root of the tree
	parentIndex := h.getParentIndex(index)
	if parentIndex == -1 {
		return
	}

	parent := h.values[parentIndex]
	current := h.values[index]

	if parent.Priority() > current.Priority() {
		h.values[index], h.values[parentIndex] = h.values[parentIndex], h.values[index]
	}

	h.heapifyUp(parentIndex)
}

func (h *MinHeap[T]) heapifyDown(index int) {
	leftChildIndex := h.getLeftChildIndex(index)
	rightChildIndex := h.getRightChildIndex(index)

	// value is already a leaf
	if leftChildIndex == -1 && rightChildIndex == -1 {
		return
	}

	current := h.values[index]
	minChild := h.values[leftChildIndex]
	minChildIndex := leftChildIndex
	if rightChildIndex != -1 {
		rightChild := h.values[rightChildIndex]

		if rightChild.Priority() < minChild.Priority() {
			minChild = rightChild
			minChildIndex = rightChildIndex
		}
	}

	if current.Priority() > minChild.Priority() {
		h.values[index], h.values[minChildIndex] = h.values[minChildIndex], h.values[index]
	}

	h.heapifyDown(minChildIndex)
}

func (h *MinHeap[T]) getLeftChildIndex(parentIndex int) int {
	leftChildIndex := 2*parentIndex + 1
	if len(h.values)-1 < leftChildIndex {
		return -1
	}

	return leftChildIndex
}

func (h *MinHeap[T]) getRightChildIndex(parentIndex int) int {
	rightChildIndex := 2*parentIndex + 2
	if len(h.values)-1 < rightChildIndex {
		return -1
	}

	return rightChildIndex
}

func (h *MinHeap[T]) getParentIndex(childIndex int) int {
	// root can't have parent
	if childIndex == 0{
		return -1
	}
	parentIndex := childIndex / 2
	if len(h.values)-1 < parentIndex {
		return -1
	}

	return parentIndex
}
