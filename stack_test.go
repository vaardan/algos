package algos

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Stack(t *testing.T){
	stack := NewStack[string]()

	// three <-
	stack.Push("three")
	assert.Equal(t, 1, stack.Len())
	assert.Equal(t, "three", *stack.Peek())

	// three <- two <-
	stack.Push("two")
	assert.Equal(t, 2, stack.Len())
	assert.Equal(t, "two", *stack.Peek())

	// three <- two <- one <-
	stack.Push("one")
	assert.Equal(t, 3, stack.Len())
	assert.Equal(t, "one", *stack.Peek())

	// three <- two <- | one
	assert.Equal(t, "one", *stack.Pop())
	assert.Equal(t, 2, stack.Len())
	assert.Equal(t, "two", *stack.Peek())

	// three <- | two 
	assert.Equal(t, "two", *stack.Pop())
	assert.Equal(t, 1, stack.Len())
	assert.Equal(t, "three", *stack.Peek())

	// (empty stack) <- | three
	assert.Equal(t, "three", *stack.Pop())
	assert.Equal(t, 0, stack.Len())
	assert.Nil(t, stack.Peek())
}
