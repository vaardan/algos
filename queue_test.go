package algos

import (
	"fmt"
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
)

func Test_Queue(t *testing.T){
	queue := NewQueue[string]()
	fmt.Printf("queue <state:0>: %s", spew.Sdump(queue))

	// "one" 
	queue.Enqueue("one")
	assert.Equal(t, 1, queue.Len())
	assert.Equal(t, "one", *queue.Peek())

	// "one" -> "two"
	queue.Enqueue("two")
	assert.Equal(t, 2, queue.Len())
	assert.Equal(t, "one", *queue.Peek())

	// "one" -> "two" -> "three"
	queue.Enqueue("three")
	assert.Equal(t, 3, queue.Len())
	assert.Equal(t, "one", *queue.Peek())

	// "one" | "two" -> "three"
	assert.Equal(t, "one", *queue.Deque())
	assert.Equal(t, 2, queue.Len())
	assert.Equal(t, "two", *queue.Peek())

	// "two" | "three"
	assert.Equal(t, "two", *queue.Deque())
	assert.Equal(t, 1, queue.Len())
	assert.Equal(t, "three", *queue.Peek())

	// "three" | (empty queue)
	assert.Equal(t, "three", *queue.Deque())
	assert.Equal(t, 0, queue.Len())
	assert.Nil(t, queue.Peek())
}
