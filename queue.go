package algos

type queueNode[T any] struct {
	value T
	next  *queueNode[T]
}

// Queue is a FIFO data structure.
// It's usually implemented using a linked list, because it's allows O(1) dequeue operation.
// Unlike in the array list all of the following elements don't have to be shifted left at the O(n).
type Queue[T any] struct {
	length int
	head   *queueNode[T]
	tail   *queueNode[T]
}

func NewQueue[T any]() *Queue[T] {
	return &Queue[T]{
		length: 0,
	}
}

func (q *Queue[T]) Enqueue(v T) {
	newNode := &queueNode[T]{value: v}
	defer func() {
		q.length++
	}()

	if q.length == 0 {
		q.head = newNode
		q.tail = newNode
		return
	}

	previousNode := q.tail
	q.tail = newNode
	previousNode.next = newNode
}

func (q *Queue[T]) Deque() *T {
	if q.length == 0 {
		return nil
	}

	q.length--

	dequedNode := q.head
	if dequedNode.next == nil {
		q.head = nil
		q.tail = nil
		return &dequedNode.value
	}

	q.head = dequedNode.next
	return &dequedNode.value
}

func (q *Queue[T]) Peek() *T {
	if q.length == 0 {
		return nil
	}

	return &q.head.value
}

func (q *Queue[T]) Len() int {
	return q.length
}
