package algos

type stackNode[T any] struct {
	value    T
	previous *stackNode[T]
}

// Stack is a LIFO data structure.
// Can be implemented using a linked list or an array list.
// Pop operations are O(1) in either data structures.
// Push is O(1) when a linked list is used; and O(n) if the capacity of the stack was surpassed.
type Stack[T any] struct {
	length  int
	current *stackNode[T]
}

func NewStack[T any]() *Stack[T] {
	return &Stack[T]{
		length: 0,
	}
}

func (s *Stack[T]) Push(v T) {
	newNode := &stackNode[T]{value: v}
	defer func() {
		s.length++
	}()

	if s.length == 0 {
		s.current = newNode
		return
	}

	previousNode := s.current
	s.current = newNode
	newNode.previous = previousNode
}

func (s *Stack[T]) Pop() *T {
	if s.length == 0 {
		return nil
	}
	s.length--

	poppedNode := s.current
	if poppedNode.previous == nil {
		s.current = nil
		return &poppedNode.value
	}

	s.current = poppedNode.previous
	return &poppedNode.value
}

func (s *Stack[T]) Peek() *T {
	if s.length == 0 {
		return nil
	}

	return &s.current.value
}

func (s *Stack[T]) Len() int {
	return s.length
}
