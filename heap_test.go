package algos

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type proritazibleInt int

func (w proritazibleInt) Priority() int {
	return int(w)
}

func Test_Heap(t *testing.T) {

	heap := NewMinHeap[proritazibleInt]()

	heap.Push(proritazibleInt(420))
	assert.EqualValues(t, 420, *heap.Peek())
	assert.Equal(t, 1, heap.Len())

	heap.Push(proritazibleInt(69))
	assert.EqualValues(t, 69, *heap.Peek())
	assert.Equal(t, 2, heap.Len())

	heap.Push(proritazibleInt(5))
	assert.EqualValues(t, 5, *heap.Peek())
	assert.Equal(t, 3, heap.Len())

	heap.Push(proritazibleInt(2))
	assert.EqualValues(t, 2, *heap.Peek())
	assert.Equal(t, 4, heap.Len())

	v := heap.Pop()
	assert.NotNil(t, v)
	assert.EqualValues(t, *v, 2)
	assert.EqualValues(t, 5, *heap.Peek())
	assert.Equal(t, 3, heap.Len())

	v = heap.Pop()
	assert.NotNil(t, v)
	assert.EqualValues(t, *v, 5)
	assert.EqualValues(t, 69, *heap.Peek())
	assert.Equal(t, 2, heap.Len())

	v = heap.Pop()
	assert.NotNil(t, v)
	assert.EqualValues(t, *v, 69)
	assert.EqualValues(t, 420, *heap.Peek())
	assert.Equal(t, 1, heap.Len())

	v = heap.Pop()
	assert.NotNil(t, v)
	assert.EqualValues(t, *v, 420)
	assert.Nil(t, heap.Peek())
	assert.Equal(t, 0, heap.Len())
}
